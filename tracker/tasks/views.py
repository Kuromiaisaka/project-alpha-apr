from django.shortcuts import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task 
    template_name = "task/create.html"
    fields = ['name', 'start_date', 'due_date', 'project', 'assignee']
    
    def form_invalid(self, form):
        print(form)
    def form_valid(self, form):
        item = form.save(commit=False)
        item.assignee = self.request.user
        item.save()
        return HttpResponseRedirect(reverse_lazy('show_my_tasks'))

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "task/list.html"
    fields = ['name', 'start_date', 'due_date', 'project']
    context_object_name = "tasktask"
    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)

class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ['is_completed']
    success_url = reverse_lazy("show_my_tasks")

    def form_invalid(self, form):
        print(form)
    def form_valid(self, form):
        item = form.save(commit=False)
        item.assignee = self.request.user
        item.save()
        return HttpResponseRedirect(reverse_lazy('show_my_tasks'))

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)