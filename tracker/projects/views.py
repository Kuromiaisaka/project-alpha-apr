from django.shortcuts import HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "project/list.html"
    fields = ['name', 'description']
    context_object_name = "projectnames"

class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "project/detail.html"
    fields = ['name', 'description']
    context_object_name = "projec"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['show_project'] = Project.objects.all()
        return context
    
    def get_object(self):
        return Project.objects.get(id=self.kwargs.get("pk"))

class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "project/create.html"
    fields = ['name', 'description']

    def form_valid(self, form):
        item = form.save(commit=False)
        item.memberes = self.request.user
        item.save()
        return HttpResponseRedirect(reverse_lazy('show_project', args=[item.id]))

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)