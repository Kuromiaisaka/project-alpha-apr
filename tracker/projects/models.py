from django.db import models
from django.conf import settings
from tasks.models import Task
# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL

class Project(models.Model):
    name = models.CharField(max_length= 200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")
    
    def get_tasks(self):
        return Task.objects.filter(project=self.pk)

    def __str__(self):
        return self.name